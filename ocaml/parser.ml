open ASD
open Token
let parse_object = parser
    [<'ENTITY ent>] -> Ent_obj (Ent ent)
   |[<'STRING str>] -> Id str
and parse_entity = parser
    [<'ENTITY ent>] -> Ent ent
let rec parse_objects = parser
                      |[<h=parse_object; t= (tail_exp_obj h)>] -> t
and tail_exp_obj h=
  parser
|[<'COMMA; t=parse_objects>] -> h::t
|[<>] -> [h]
let rec parse_predicats = parser
                        |[<h=parse_predicat; t=(tail_exp_pred h)>] -> t
and tail_exp_pred h=
  parser [<'SEMICOLON; t=parse_predicats>] -> h::t
|[<>] -> [h]
and parse_predicat  = parser
    [<ent=parse_entity; l=parse_objects??"predicat list expected">] -> ({pred_id=ent; objects=l}:predicat)
let rec parse_subjects = parser
    [< h=parse_subjet;'POINT??"point expected" ;t=parse_subjects??"subject tail expected">] -> (h:subject)::t
   |[<>] -> []
and parse_subjet = parser
  [<ent=parse_entity; l=parse_predicats??"subject list expected">] -> ({subject_id=ent; predicats=l}:subject) 
let parse = parser
  [<exp=parse_subjects>] -> Turtle exp
